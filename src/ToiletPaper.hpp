#include "GameObject.hpp"
#include <Box2D/Box2D.h>
#include <jngl.hpp>

class ToiletPaper : public GameObject {
public:
	ToiletPaper(b2World& world, jngl::Vec2 position);
	~ToiletPaper();


	void draw() const override;
	bool step() override;
    void onContact(GameObject*) override;

private:
	jngl::Sprite sprite{"tp"};
    bool destroy = false;
};
